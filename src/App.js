import MyList from './myList/myList.js';
import MyList2 from './myList2/myList2.js';

function App() {
  return (
    <div className="App">

      <h1>MYLIST REACTJS</h1>

      <h2>Function version</h2>
      <MyList shadow="true"></MyList>
      
      <h2>Class version</h2>
      <MyList2 shadow="false"></MyList2>
    </div>
  );
}

export default App;
