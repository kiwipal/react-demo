import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/MYLIST REACTJS/i);
  expect(linkElement).toBeInTheDocument();
});

it("renders without crashing", () => {
  mount(<App />);
});

it("renders Account header", () => {
  const wrapper = mount(<App />);
  const welcome = <h1>MYLIST REACTJS</h1>;
  expect(wrapper.contains(welcome)).toEqual(true);
});

