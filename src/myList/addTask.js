import { PropTypes } from "prop-types";

AddTask.propTypes = {
    newTask: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired
};

export function AddTask(props) {

    const {newTask, onSubmit, onChange} = props;

    return (
        
        <form className="form_new_task" onSubmit={onSubmit}>

            <div className="new_task">
                <input
                    name="title"
                    className="title"
                    type="text"
                    value={newTask.title}
                    placeholder="Add a task ..."
                    autoComplete="off"
                    onChange={onChange}
                />
                {!newTask.title || <button className="button_add">ADD</button>}
            </div>

            {!newTask.title || (
                <textarea
                    name="description"
                    className="description"
                    value={newTask.description}
                    placeholder="Description ..."
                    rows="3"
                    onChange={onChange}
                ></textarea>)}

        </form>
    )
}