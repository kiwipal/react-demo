import { useState, useEffect} from 'react'
import { PropTypes } from "prop-types";
import { AddTask } from './addTask';
import { TasksList } from './tasksList';
import './myList.scss';

MyList.propTypes = {
    shadow: PropTypes.string
};

MyList.defaultProps = {
    shadow: 'true',
    default_task: {
        id: '',
        title: '',
        description: ''
    }
}

export default function MyList(props) {

    const defaultTask = props.default_task;
    const [newTask, setNewTask] = useState(defaultTask);
    const [list, setList] = useState([]);

    // @param obj event
    const handleNewTaskSubmit = (event) => {
        event.preventDefault();
        if (newTask.title) {
            setList(prevState => [newTask, ...prevState])
            resetNewTask();
        }
    }

    // @param obj target
    const handleNewTaskChange = ({target}) => {
        const {name, value} = target;
        setNewTask( prevState => ({
            ...prevState,
            id: Date.now(),
            [name]: value
        }));
    }

    // @param int id task
    const handleDeleteTask = (idTask) => {
        setList(prevState => prevState.filter(task => task.id !== idTask))
    }

    const resetNewTask = () => {
        setNewTask(defaultTask);
    }

    // @param boolean shadow
    // @return string (list of classnames with space separators)
    const getClassNames = (hasShadow) => {
        const classNames = ["mylist"];
        hasShadow && classNames.push("shadow");
        return classNames.join(" ");
    }

    // @param integer totalTasks number
    // @return string
    const createDocumentTitle = (totalTasks) => {
        const text = totalTasks === 1 ? "task" : "tasks";
        return `Mylist 2 ▸ ${totalTasks} ${text}`;
    }

    useEffect(
        () => { document.title = createDocumentTitle(list.length) },
        [list]
    );

    return (
        <div className={ getClassNames(props.shadow) }>
            <AddTask
                newTask={newTask}
                onSubmit={handleNewTaskSubmit}
                onChange={handleNewTaskChange}
            ></AddTask>
            <TasksList
                list={list}
                onDelete={handleDeleteTask}
            ></TasksList>
        </div>
    )
}