import { PropTypes } from "prop-types";

TasksList.propTypes = {
    list: PropTypes.array.isRequired,
    onDelete: PropTypes.func.isRequired
};

export function TasksList(props) {

    const {list, onDelete} = props;

    if (list.length) {

        return <ul className="tasks_list">{
            list.map((task, index) => (
                <li className="task" key={"task_" + index}>
                    <p className="title">{task.title}</p>
                    <p className="description">{task.description}</p>
                    <span className="button_delete_task" onClick={() => onDelete(task.id)}>×</span>
                </li>
            )
        )}</ul>

    }else{
        return <div className="tasks_list_empty">Please create your first task!</div>
    }
    
}