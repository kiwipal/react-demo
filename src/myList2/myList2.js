import React from 'react';
import { PropTypes } from "prop-types";
import './myList2.scss';

export default class Mylist2 extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            shadow: (this.props.shadow === "true"),
            default_task: this.props.default_task,
            new_task: this.props.default_task,
            list: []
        }
        
        this.handleNewTaskChange = this.handleNewTaskChange.bind(this);
        this.handleNewTaskSubmit = this.handleNewTaskSubmit.bind(this);

    }
    
    resetNewTask() {
        this.setState(
            (prevState) => {
                return {
                    ...prevState,
                    new_task: this.state.default_task
                }
            }
        );
    }

    handleNewTaskSubmit(event) {

        event.preventDefault();

        if (this.state.new_task.title) {

            const list = this.state.list;
            list.push(this.state.new_task);
    
            this.setState(
                (prevState) => {
                    return {
                        ...prevState,
                        list: list
                    }
                }
            )
    
            this.resetNewTask();
        }
        
    }

    // @param obj target
    handleNewTaskChange({target}) {

        const {name, value} = target;

        this.setState(
            (prevState) => {
                return {
                    ...prevState,
                    new_task: {
                        ...prevState.new_task,
                        [name]: value,
                        id: Date.now()
                    }
                }
            }
        )
        
    }

    // @param int id task
    handleDeleteTask(idTask) {
        
        const newList = this.state.list.filter(
            (task) => task.id !== idTask
        );

        this.setState(
            (prevState) => {
                return {
                    ...prevState,
                    list: newList
                }
            }
        );

    }

    // @param integer tasks number
    // @return string
    createDocumentTitle(totalTasks) {
        const text = totalTasks === 1 ? "task" : "tasks";
        return `Mylist 2 ▸ ${totalTasks} ${text}`;
    }

    componentDidUpdate() {
        document.title = this.createDocumentTitle(this.state.list.length);
    }

    render () {
        return (
            <div className={this.state.shadow ? 'mylist shadow' : 'mylist'}>

                <form className="form_new_task" onSubmit={this.handleNewTaskSubmit}>

                    <div className="new_task">
                        <input
                            name="title"
                            className="title"
                            type="text"
                            value={this.state.new_task.title}
                            placeholder="Add a task ..."
                            autoComplete="off"
                            onChange={this.handleNewTaskChange}
                        />
                        {!this.state.new_task.title || <button className="button_add">ADD</button>}
                    </div>

                    {!this.state.new_task.title || (
                        <textarea
                            name="description"
                            className="description"
                            value={this.state.new_task.description}
                            placeholder="Description ..."
                            rows="3"
                            onChange={this.handleNewTaskChange}
                        ></textarea>)}

                </form>

                {this.state.list.length
                    ? (
                        <ul className="tasks_list">
                            {
                                this.state.list.map((task, index) => (
                                    <li className="task" key={"task_" + index}>
                                        <p className="title">{task.title}</p>
                                        <p className="description">{task.description}</p>
                                        <span className="button_delete_task" onClick={() => this.handleDeleteTask(task.id)}>×</span>
                                    </li>)
                                )
                            }
                        </ul>
                    )
                    : (<div className="tasks_list_empty">Please create your first task!</div>)}

            </div>
        )
    }

}

Mylist2.propTypes = {
    shadow: PropTypes.string
};

Mylist2.defaultProps = {
    shadow: 'true',
    default_task: {
        id: '',
        title: '',
        description: ''
    }
}